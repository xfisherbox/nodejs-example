var express = require("express");
var app = express();

app.get("/", function (req, res) {
  res.send("Hello from node.js!");
});

var server = app.listen(8000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log(`Listening http://${host}:${port}`);
});
